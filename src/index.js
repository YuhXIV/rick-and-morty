import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import CardPage from './Containers/CharacterCards/CardPage/CardPage';
import CardsPage from './Containers/CharacterCards/CardsPage/CardsPage';
import Header from './Components/Layout/Header';
import Footer from './Components/Layout/Footer';
import FrontPage from './Components/Layout/FrontPage';
import LocationsPage from './Containers/Location/LocationsPage';
import LocationPage from './Containers/Location/LocationPage.js';

ReactDOM.render(
    <Router>
        <App>
            <Route path='/' component={Header} />
            <Route exact path='/' component={FrontPage} />
            <div className="container pb-32 main-content">
                <Route exact path='/' component={CardsPage} />
                <Route path='/character/:id' component={CardPage} />
                <Route exact path='/location' component={LocationsPage}></Route>
                <Route path='/location/:id' component={LocationPage}></Route>
            </div>
            <Route path='/' component={Footer} />
        </App>
    </Router>,
    document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
