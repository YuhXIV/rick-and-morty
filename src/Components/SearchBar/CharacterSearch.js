import React from 'react';

const HandleCharacterSearch = (props) => {
    const searchInput = React.createRef();


    let sendSearchQuery = () => {
        props.searchInfo(searchInput.current.value);
    }

    let newSearchQuery = () => {
        props.newSearchQuery(searchInput.current.value);
    }

    return (
        <React.Fragment>
            <div className='search offset-3 col-6'>
                <input type='text'
                    ref={searchInput}
                    onChange={sendSearchQuery}
                    className='form-control'
                    placeholder='Search' />
            </div>
            <div>
                <button
                    className='btn btn-primary' onClick={newSearchQuery}> New query </button>
            </div>
        </React.Fragment>

    )
}

export default HandleCharacterSearch;