import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="App">
          <div>
            {this.props.children}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
