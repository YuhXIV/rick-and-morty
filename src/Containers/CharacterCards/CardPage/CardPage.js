import React from 'react';

import CharacterCard from '../../../Components/CharacterCard/CharacterCard';

const api_url = 'https://rickandmortyapi.com/api/character/';

class CardPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            character: null,
        }
    }

    componentDidMount() {
        this.getCharacterData();
    }

    getCharacterData() {
        fetch(api_url + this.props.match.params.id).then(response => response.json())
            .then(data => {
                if (data) {
                    this.setState({
                        character: data,
                    })
                }
            });
    }

    render() {
        let character = null;

        if (this.state.character !== null && this.state.character !== undefined) {
            character =
                <CharacterCard key={this.state.character.id}
                    {...this.state.character}
                />

        } else {
            character = (
                <div className='col-12 loader-position mt-5'>
                    <div className="spinner-grow text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <div className='row'>
                    <div className='col-12 singel-character-card'>
                        {character}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default CardPage