import React from 'react';
import LocationCard from '../../Components/LocationCard/LocationCard';
import HandleCharacterSearch from './../../Components/SearchBar/CharacterSearch';

const api_url = 'https://rickandmortyapi.com/api/';
const query = 'location/';

export default class LocationsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
            searchLocations: [],
        }
    }

    componentDidMount() {
        this.getData();
    }

    searchBar = (input) => {
        let filterLocations = null;

        filterLocations = this.state.locations.filter(location => location.name.toLowerCase().includes(input)
            || location.type.toLowerCase().includes(input)
            || location.dimension.toLowerCase().includes(input));
        if (filterLocations.length > 0) {
            this.setState({ searchLocations: filterLocations });
        } else {
            this.setState({ searchLocations: this.state.locations });
        }
    }

    newQuery = (input) => {
        fetch(api_url + query + '?name=' + input).then(response => response.json())
            .then(data => this.setState({
                searchLocations: data.results,
                locations: data.results,
            }));
    }

    getData() {
        fetch(api_url + query)
            .then(response => response.json())
            .then(data => this.setState({
                locations: data.results,
                searchLocations: data.results,
            }));
    }

    render() {
        let locations = null;

        if (this.state.searchLocations !== undefined && this.state.searchLocations.length > 0) {
            locations = this.state.searchLocations.map(location => (
                <LocationCard
                    key={location.id}
                    {...location}
                    showLink={true} />
            ));
        } else {
            locations = (
                <div className='col-12 loader-position mt-5'>
                    <div className="spinner-grow text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <h4 className='mt-5'> Locations </h4>
                <div className='row'>
                    <HandleCharacterSearch
                        searchInfo={this.searchBar}
                        newSearchQuery={this.newQuery} />
                </div>
                <br />
                <div className='row'>
                    {locations}
                </div>

            </React.Fragment>
        )
    }
}
