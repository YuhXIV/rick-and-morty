import React from 'react';
import CharacterCard from '../../../Components/CharacterCard/CharacterCard';
import HandleCharacterSearch from '../../../Components/SearchBar/CharacterSearch';

const api_url = 'https://rickandmortyapi.com/api/';
const query = 'character/';

export default class CardsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rickMorty: [],
            characterCards: [],
            filterCharacter: [],
        }
    }

    componentDidMount() {
        this.getData();
    }

    searchBar = (input) => {
        let filterCharacter = null;
        filterCharacter = this.state.characterCards.filter(character => character.name.toLowerCase().includes(input));
        if (filterCharacter.length > 0) {
            this.setState({ filterCharacter: filterCharacter });
        } else {
            this.setState({ filterCharacter: this.state.characterCards });
        }
    }

    newQuery = (input) => {
        fetch(api_url + query + '?name=' + input).then(response => response.json())
            .then(data => this.setState({
                characterCards: data.results,
                filterCharacter: data.results,
            }));
    }

    getData() {
        fetch(api_url + query)
            .then(response => response.json())
            .then(data => this.setState({
                characterCards: data.results,
                filterCharacter: data.results,
            }));
    }

    render() {
        let filterCharacter = null;

        if (this.state.characterCards !== undefined && this.state.characterCards.length > 0) {
            filterCharacter = this.state.filterCharacter.map(character => (
                <CharacterCard
                    key={character.id}
                    {...character}
                    showLink={true} />
            ));
        } else {
            filterCharacter = (
                <div className='col-12 loader-position mt-5'>
                    <div className="spinner-grow text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <h4 className='mt-5'> Rick & Morty characters</h4>
                <br />
                <div className='row'>
                    <HandleCharacterSearch
                        searchInfo={this.searchBar}
                        newSearchQuery={this.newQuery} />
                </div>
                <div className="row">
                    {filterCharacter}
                </div>
            </React.Fragment>
        )
    }
}

//export default CardsPage;