import React from 'react';

import LocationCard from './../../Components/LocationCard/LocationCard';

const api_url = 'https://rickandmortyapi.com/api/location/';

class LocationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            location: null,
        }
    }

    componentDidMount() {
        this.getLocationData();
    }

    getLocationData() {
        fetch(api_url + this.props.match.params.id).then(response => response.json())
            .then(data => {
                if (data) {
                    this.setState({
                        location: data,
                    })
                }
            });
    }

    render() {
        let location = null;

        if (this.state.location !== null && this.state.location !== undefined) {
            location =
                <LocationCard key={this.state.location.id}
                    {...this.state.location}
                />

        } else {
            location = (
                <div className='col-12 loader-position mt-5'>
                    <div className="spinner-grow text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <div className='row'>
                    <div className='col-12 singel-character-card'>
                        {location}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default LocationPage