import React from 'react';

const FrontPage = (props) => {

    return (
        <header className="App-header">
            <img
                src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
                alt="Frontpage removed from source"
                style={{
                    margin: '0',
                    width: '100%',
                    display: 'block',
                }}
            />
        </header>
    )
}

export default FrontPage;

