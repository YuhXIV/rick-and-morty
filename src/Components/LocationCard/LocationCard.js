import React from 'react';

import { NavLink } from 'react-router-dom'

const LocationCard = (props) => {
    let locationInfo = null;
    let linkButton = null;
    if (props.showLink) {
        linkButton = <NavLink to={{ pathname: '/location/' + props.id, }}
            className="btn btn-primary">View</NavLink>;

        //     locationInfo = (
        //         <div className='card-img'>
        //             <img src={props.image} alt={props.name} className='card-img-top' />
        //             <div className='card-body'>
        //                 <NavLink to={{ pathname: '/character/' + props.id, }}
        //                     className="character-name" >{props.name}</NavLink>
        //             </div>
        //         </div>
        //     )
    }
    locationInfo = (
        <div className='card-img'>
            <div className='card-info'>
                <b>Name: </b> {props.name} <br />
                <b>Planet: </b> {props.type} <br />
                <b>Dimension: </b> {props.dimension} <br />
                {linkButton}
            </div>
        </div>
    )

    return (
        <div className='col-xs-12 col-sm-6 col-md-4 mt-4'>
            <div className='card bg-light text-dark'>
                {locationInfo}
            </div>
        </div>
    )
}

export default LocationCard;