import React from 'react';
import './CharacterCard.scss';

import { NavLink } from 'react-router-dom'

const CharacterCard = (props) => {
    let characterInfo = null;
    if (props.showLink) {
        characterInfo = (
            <div className='card-img'>
                <img src={props.image} alt={props.name} className='card-img-top' />
                <div className='card-body'>
                    <NavLink to={{ pathname: '/character/' + props.id, }}
                        className="character-name" >{props.name}</NavLink>
                </div>
            </div>
        )
    } else {
        let originId = props.origin.url.substring(props.origin.url.lastIndexOf('/') + 1);
        let locationId = props.location.url.substring(props.location.url.lastIndexOf('/') + 1);

        characterInfo = (
            <div className='card-img'>
                <img src={props.image} alt={props.name} className='card-img-top' />
                <div className='card-info'>
                    <b>Species: </b> {props.species} <br />
                    <b>Status: </b> {props.status} <br />
                    <b>Gender: </b> {props.gender} <br />
                    <b>Location: </b> <NavLink to={{ pathname: '/location/' + locationId }} > {props.location.name} </NavLink> <br />
                    <b>Place of origin: </b> <NavLink to={{ pathname: '/location/' + originId }} > {props.origin.name} </NavLink> <br />
                </div>
            </div>

        )
    }

    return (
        <div className='col-xs-12 col-sm-6 col-md-4 mt-4'>
            <div className='card bg-light text-dark'>
                {characterInfo}
            </div>
        </div>
    )
}

export default CharacterCard;