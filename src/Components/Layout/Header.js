import React from 'react';

import { NavLink } from 'react-router-dom'

const Header = (props) => {
    return (
        <header>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <NavLink to={{ pathname: '/' }} className="navbar-brand" >R&M </NavLink>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item">
                            <NavLink to={{ pathname: '/' }} exact activeClassName='current'
                                className="nav-link" >
                                Home
                            </NavLink>
                        </li>
                        <li className='nav-item'>
                            <NavLink to={{ pathname: '/location' }} exact activeClassName='current'
                                className="nav-link" >
                                Location
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}

export default Header;

